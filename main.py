import os
import time
import re
from messageHandler import handle_message
from slackclient import SlackClient

# instantiate Slack client
slack_client = SlackClient(os.environ.get('slackbot_token'))
# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None

# constants
RTM_READ_DELAY = 1  # 1 second delay between reading from RTM


def parse_text(slack_events):
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            handle_message(event["text"], event["channel"], slack_client)
    return None, None


if __name__ == "__main__":
    if slack_client.rtm_connect(with_team_state=False):
        print("Kanna is alive!")
        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call("auth.test")["user_id"]
        while True:
            parse_text(slack_client.rtm_read())
            time.sleep(RTM_READ_DELAY)
    else:
        print("Connection failed. Exception traceback printed above.")
