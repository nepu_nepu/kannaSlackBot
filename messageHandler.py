import random

def handle_message(message, channel, slack_client):
    response = None
    # This is where you start to implement more commands!
    tokens = message.split(' ')
    msg = message.lower()

    if msg.startswith("hi "):
        responses = ["Hello there!",
                     "Greetings",
                     "but what do you mean?!"
                     ]
        choice = random.randint(0, len(responses)-1)
        response = responses[choice]
    elif msg.startswith("rain"):
        responses = ["Ame ame ame",
                     "(ᗒᗩᗕ)"
                     ]
        choice = random.randint(0, len(responses)-1)
        response = responses[choice]
    elif msg.startswith("hello there"):
        responses = ["General Kenobi!",
                     "Hai dere",
                     u"\u00AD>w<",
                     "ヾ(＾-＾)ノ"
                     ]
        choice = random.randint(0, len(responses)-1)
        response = responses[choice]
    elif msg.startswith("rand"):
        if len(tokens) == 3:
            num1 = tokens[1]
            num2 = tokens[2]
            try:
                num1 = int(num1)
                num2 = int(num2)
                response = str(random.randint(
                    min([num1, num2]), max([num1, num2])
                ))
            except:
                response = "wat"
    elif msg.startswith("my name is") and len(tokens) > 3:
        name = " ".join(tokens[3:])
        responses = ["Hi there {}".format(name),
                     "{} smells!".format(name),
                     "Will you be my friend {}?".format(name),
                     "Hi {}, I'm Kanna! /w\\".format(name)
                     ]
        choice = random.randint(0, len(responses)-1)
        response = responses[choice]
    elif msg.startswith("morning") or msg.startswith("good morning"):
        responses = ["Good morning!",
                     "Ohayou",
                     "Guten Morgen",
                     "It is morning!",
                     "G'day maaaate!"
                     ]
        choice = random.randint(0, len(responses)-1)
        response = responses[choice]
    elif "harrison" in msg:
        responses = ["Praise the lord!",
                     "> That which is Harrison is amazing\n- Barack Obama",
                     "That is master"
                     ]
        choice = random.randint(0, len(responses)-1)
        response = responses[choice]
    elif "ajitha" in msg:
        responses = ["Big Boss!",
                     "Boss, boss!",
                     "I want Jack Fruit!"
                     ]
        choice = random.randint(0, len(responses)-1)
        response = responses[choice]
    elif "daniel" in msg:
        responses = ["Friend! Friend!",
                     "Bring snacks",
                     "I hope I don't fail the NAPLAN"
                     ]
        choice = random.randint(0, len(responses)-1)
        response = responses[choice]
    elif "doctor" in msg:
        responses = ["MEDIC!",
                     "I need healing",
                     "*cough*"
                     ]
        choice = random.randint(0, len(responses)-1)
        response = responses[choice]

    # Sends the response back to the channel
    if response:
        slack_client.api_call(
            "chat.postMessage",
            channel=channel,
            text=response
        )